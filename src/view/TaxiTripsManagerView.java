package view;

import java.util.Scanner;

import controller.Controller;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					System.out.println("Ingrese el Id del taxi para la carga de datos del subconjunto de datos small de servicios:");
					String taxiId = sc.next();
					Controller.loadServices( taxiId );
					break;
				case 2:
					int [] resultadoOrdenInv = Controller.servicesInInverseOrder();
					
					System.out.println("Total de servicios en orden inverso de tiempo de inicio "+ resultadoOrdenInv[0]);
					System.out.println("Total de servicios que No estan en orden inverso de tiempo de inicio "+ resultadoOrdenInv[1]);
										
					break;
				case 3:
					int [] resultadoOrden = Controller.servicesInOrder();
					
					System.out.println("Total de servicios en orden de tiempo de inicio "+ resultadoOrden[0]);
					System.out.println("Total de servicios que No estan en orden de tiempo de inicio "+ resultadoOrden[1]);

					break;
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Cargar los servicios de un taxi reportados en un subconjunto de datos");
		System.out.println("2. Verificar el ordenamiento inverso de los servicios del taxi por tiempo de inicio");
		System.out.println("3. Verificar el ordenamiento de los servicios del taxi por tiempo de inicio");
		System.out.println("4. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
